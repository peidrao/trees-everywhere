run:
	docker-compose up -d

logs:
	docker attach trees-everywhere-web-1

bash:
	docker exec -it trees-everywhere-web-1 bash

migrate:
	docker exec -it trees-everywhere-web-1 python manage.py migrate

admin:
	docker exec -it trees-everywhere-web-1 python manage.py createadmin

seed:
	docker exec -it trees-everywhere-web-1 python manage.py seed

test:
	docker exec -it trees-everywhere-web-1 python manage.py test --keepdb

coverage:
	docker exec -it trees-everywhere-web-1 coverage run manage.py test --keepdb

html:
	docker exec -it trees-everywhere-web-1 coverage html
