# Trees Everywhere

## Considerações sobre o projeto

- Todos ou sua grande maioria de requisitos do desafio foram atendidas
- O projeto foi dockerizado por completo.
- Para os templates, foi usado bootstrap e um template que já havia implementando em outro projeto de e-commerce.
  - VirgilBookStore (meu projeto) - [Link](https://github.com/peidrao/VirgilBookStore)
- A maioria das requisições do tipo POST foram feitas usando AJAX e jquery.
  - O intuito disso foi trazer maior flexibilidade e agilidade em possíveis refatorações ou em usar frameworks web mais sofisticados como react
- Foi adicionado o SweetAlert2 na geração de feedbacks para o usuário e garantir uma melhor usabilidade
- No campo dos padrões de projeto, foi adicionado o pre-commit, onde garantiremos uma melhor padronização do nosso código. Isso tudo antes de enviarmos para nosso repositório remoto.
  - Também foi adicionado o Makefile, onde teremos uma maior agilidade em executarmos alguns comandos.

## Como usar

> ⚠ **Atenção**
>
> É necessário ter o docker e o docker compose instalados em seu computador.

```bash
# Clone o repositorio
$ git clone https://gitlab.com/peidrao/trees-everywhere.git
# entre no diretorio
$ cd trees-everywhere
# Instalando projeto
$ make run
# Vendo se o projeto fez o start
$ make logs
# Criacao de migracoes no banco de dados
$ make migrate
```

## Populando o banco de dados

Para gerar uma dinamicidade maior na hora dos testes você poderá popular seu banco de dados.

```bash
# Para criar super usuario
$ make admin
# Criar contas e plantas
$ make seed
```

As credenciais do usuário administrador são:

| nome de usuário | senha |
| --------------- | ----- |
| admin           | 123   |

## Testes unitários

O projeto tem cobertura de 96%, ou seja, para quase todos os serviços desenvolvidos, sejam templates ou apis existem testes unitários.

```bash
# Rodar os testes
$ make test
# Verificar cobertura
$ make coverage
# Gerar arquivos html
$ make html
```

> ⚠ **Atenção**
>
> O make html irá gerar um diretório dentro do nosso projeto chamado **htmlcov**. Dentro dele existem vários arquivos html, e entre eles o **index.html**, basta abrir o mesmo no navegador e teremos a porcentagem mais arquivos que estão cobertos.

## Endpoints

- `GET /api/tree/me` - Retorna uma lista de todas as árvores plantadas pelo usuário logado
- `POST /api/plant/` - Cria plantas
- `POST /api/tree` - Planta uma nova árvore
- `POST /api/users/` - Cria usuários
- `POST /api/accounts/` - Cria contas
- `POST /api/accounts/{id}/active` - Atualiza o status de uma conta
- `POST /api/users/update/` - Atualiza o "sobre" do perfil atrelado a um usuário
