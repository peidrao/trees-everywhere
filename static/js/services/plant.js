$("#form-plant-create").on("submit", function (e) {
  e.preventDefault();
  let form = $("#form-plant-create").serialize();

  let headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "POST",
    headers: headers,
    dataType: "json",
    data: form,
    url: "/api/plant/",
    success: function (response) {
      console.log(response);
      Swal.fire("Sucesso!", response.message, "success");
    },
    error: function (err) {
      console.log(err);
      Swal.fire({
        title: "Erro",
        icon: "warning",
        text: err.responseJSON.message,
      });
    },
  });
});

$("#form-plant-tree").on("submit", function (e) {
  e.preventDefault();
  let form = $("#form-plant-tree").serialize();

  let headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "POST",
    headers: headers,
    dataType: "json",
    data: form,
    url: "/api/tree",
    success: function (response) {
      console.log(response);
      Swal.fire("Sucesso!", response.message, "success");
    },
    error: function (err) {
      console.log(err);
      Swal.fire({
        title: "Erro",
        icon: "warning",
        text: err.responseJSON.message,
      });
    },
  });
});
