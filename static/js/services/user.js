$("#form-user-create").on("submit", function (e) {
  e.preventDefault();
  let form = $("#form-user-create").serialize();

  let headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "POST",
    headers: headers,
    dataType: "json",
    data: form,
    url: "/api/users/",
    success: function (response) {
      Swal.fire("Sucesso!", response.message, "success");
    },
    error: function (err) {
      console.log(err);
      Swal.fire({
        title: "Erro",
        icon: "warning",
        text: err.responseJSON.message,
      });
    },
  });
});

$("#form-profile-update").on("submit", function (e) {
  e.preventDefault();
  let form = $("#form-profile-update").serialize();

  let headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "PUT",
    headers: headers,
    dataType: "json",
    data: form,
    url: "/api/users/update/",
    success: function (response) {
      console.log(response);
      Swal.fire("Sucesso!", response.message, "success");
    },
    error: function (err) {
      console.log(err);
      Swal.fire({
        title: "Erro",
        icon: "warning",
        text: err.responseJSON.message,
      });
    },
  });
});
