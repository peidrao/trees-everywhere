$(".input-account-active").on("click", (e) => {
  e.preventDefault();
  let accountID = e.currentTarget.id;
  headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: `/api/accounts/${accountID}/active/`,
    headers: headers,
    success: function (response) {
      console.log(response);
      Swal.fire(response.message).then((result) => {
        if (result.isConfirmed) {
          location.reload();
        }
      });
    },

    error: function (err) {
      console.log(err.responseJSON);
    },
  });
});

$("#form-account-create").on("submit", function (e) {
  e.preventDefault();
  let form = $("#form-account-create").serialize();

  let headers = {
    "X-CSRFToken": getCookie("csrftoken"),
  };

  $.ajax({
    type: "POST",
    headers: headers,
    dataType: "json",
    data: form,
    url: "/api/accounts/",
    success: function (response) {
      console.log(response);
      Swal.fire("Sucesso!", response.message, "success");
    },
    error: function (err) {
      console.log(err);
      Swal.fire({
        title: "Erro",
        icon: "warning",
        text: err.responseJSON.message,
      });
    },
  });
});
