from account.models import Account


def get_accounts(request):
    if request.user.is_authenticated:
        accounts = Account.objects.filter(accountuser__user=request.user).distinct()
    else:
        accounts = 0
    return {"accounts": accounts}
