from django.urls import path
from tree import views

app_name = "tree"

urlpatterns = [
    path("manager/plants", views.PlantListView.as_view(), name="list_plant"),
    path(
        "manager/plants/<int:pk>/details/",
        views.PlantDetailsView.as_view(),
        name="details_plant",
    ),
    path("manager/plants/create", views.PlantCreateView.as_view(), name="create_plant"),
    path(
        "manager/tree/create", views.PlatedTreeCreateView.as_view(), name="tree_create"
    ),
    path(
        "accounts/<int:pk>/tree/",
        views.PlantedTreeByAccountView.as_view(),
        name="tree_by_accounts",
    ),
    path("tree/me", views.PlatedTreeUserView.as_view(), name="tree_me"),
    path(
        "api/tree/me/",
        views.PlatedTreeUserViewService.as_view(),
        name="tree_me_service",
    ),
    path("api/plant/", views.PlantCreateService.as_view(), name="plant_service"),
    path("api/tree", views.PlatedTreeCreateService.as_view(), name="tree_service"),
]
