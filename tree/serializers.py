from rest_framework import serializers
from account.models import PlantedTrees


class PlantedTresSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantedTrees
        fields = "__all__"
