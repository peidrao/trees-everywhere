from ast import literal_eval

from django.views import generic
from django.shortcuts import render

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from account.models import Account, PlantedTrees, Plant
from tree.serializers import PlantedTresSerializer
from core.permissions import AdministratorPermission, LoginRequiredPermission


class PlantListView(AdministratorPermission, generic.View):
    template_name = "plants/plant_table.html"

    def get(self, request):
        plants = Plant.objects.all().order_by("id")
        return render(request, self.template_name, {"plants": plants})


class PlantCreateView(AdministratorPermission, generic.TemplateView):
    template_name = "plants/plant_create.html"


class PlantDetailsView(AdministratorPermission, generic.View):
    template_name = "plants/plant_details.html"

    def get(self, request, pk):
        plants = PlantedTrees.objects.filter(plant_id=pk).order_by("id")
        return render(request, self.template_name, {"plants": plants})


class PlantedTreeByAccountView(LoginRequiredPermission, generic.View):
    template_name = "plants/plants_by_accounts.html"

    def get(self, request, pk):
        trees = PlantedTrees.objects.filter(account_id=pk, user=request.user).order_by(
            "id"
        )
        return render(request, self.template_name, {"trees": trees})


class PlantCreateService(views.APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request):
        data = request.data
        name = data.get("name")
        scientific_name = data.get("scientific_name")

        if name == "" and scientific_name == "":
            return Response(
                {"message": "Nome e Nome Científico precisam ser adicionados"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        Plant.objects.create(name=name, scientific_name=scientific_name)

        return Response(
            {"message": "Planta criada com sucesso"}, status=status.HTTP_201_CREATED
        )


class PlatedTreeUserView(LoginRequiredPermission, generic.View):
    template_name = "profile/planted_tree.html"

    def get(self, request):
        trees = PlantedTrees.objects.filter(user=request.user).distinct()
        return render(request, self.template_name, {"trees": trees})


class PlatedTreeCreateView(LoginRequiredPermission, generic.View):
    template_name = "profile/planted_create_tree.html"

    def get(self, request):
        plants = Plant.objects.all()
        accounts = Account.objects.filter(accountuser__user=request.user).distinct()

        context = {"plants": plants, "accounts": accounts}

        return render(request, self.template_name, context)


class PlatedTreeCreateService(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        location = request.data.get("location")
        age = request.data.get("age")
        plant = request.data.get("plant")
        account = request.data.get("account")
        location = tuple(literal_eval(location))
        message = ""

        if len(location) == 2 and type(location[1]) != tuple:
            user.plant_tree(plant, location, account, age)
            message = "Planta adicionada com sucesso"
        else:
            user.plant_trees(plant, location, account, age)
            message = "Plantas adicionadas com sucesso"

        return Response({"message": message}, status=status.HTTP_201_CREATED)


class PlatedTreeUserViewService(views.APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PlantedTresSerializer

    def get(self, request):
        trees = PlantedTrees.objects.filter(user=request.user).distinct()
        serializer = self.serializer_class(trees, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
