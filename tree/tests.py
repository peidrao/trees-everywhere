from django.urls import reverse

from django.test import TestCase
from account.models import Account, AccountUser, Plant, PlantedTrees
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from django.contrib.auth.models import User

from tree.views import (
    PlantCreateService,
    PlatedTreeCreateService,
    PlatedTreeUserViewService,
)


class PlatedTreeUserViewServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.account1 = Account.objects.create(name="Eagles")

        cls.user = User.objects.create(email="mahomes@chiefs.com", username="mahomes")

        AccountUser.objects.create(user=cls.user, account=cls.account1)

        cls.plant1 = Plant.objects.create(
            name="Planta 1", scientific_name="Planta Scientific 1"
        )
        cls.plant2 = Plant.objects.create(
            name="Planta 2", scientific_name="Planta Scientific 2"
        )

        PlantedTrees.objects.create(
            age=1,
            user=cls.user,
            plant=cls.plant1,
            account=cls.account1,
            latitude=12,
            longitude=12,
        )
        PlantedTrees.objects.create(
            age=2,
            user=cls.user,
            plant=cls.plant2,
            account=cls.account1,
            latitude=13,
            longitude=13,
        )

        cls.url = "api/tree/me/"

    def test_get_me_trees(self):
        view = PlatedTreeUserViewService.as_view()
        request = self.factory.get(self.url, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_me_trees_empty(self):
        user = User.objects.create(email="mm@mm.com", username="mm")

        view = PlatedTreeUserViewService.as_view()
        request = self.factory.get(self.url, format="json")
        force_authenticate(request, user=user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)


class PlatedTreeCreateServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.account1 = Account.objects.create(name="Eagles")

        cls.user = User.objects.create(email="mahomes@chiefs.com", username="mahomes")

        cls.plant1 = Plant.objects.create(
            name="Planta 1", scientific_name="Planta Scientific 1"
        )

        cls.url = "api/tree"

    def test_create_only_tree(self):
        view = PlatedTreeCreateService.as_view()
        payload = {
            "age": 2,
            "plant": self.plant1.id,
            "account": self.account1.id,
            "location": "(12, 12111)",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Planta adicionada com sucesso")
        self.assertEqual(PlantedTrees.objects.filter(user=self.user).count(), 1)

    def test_create_two_tree(self):
        view = PlatedTreeCreateService.as_view()
        payload = {
            "age": 2,
            "plant": self.plant1.id,
            "account": self.account1.id,
            "location": "(12, 12111), (12121, 112)",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Plantas adicionadas com sucesso")
        self.assertEqual(PlantedTrees.objects.filter(user=self.user).count(), 2)

    def test_create_many_tree(self):
        view = PlatedTreeCreateService.as_view()
        payload = {
            "age": 2,
            "plant": self.plant1.id,
            "account": self.account1.id,
            "location": "(12, 12111), (12121, 112), (12, 12111), (12, 12111)",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Plantas adicionadas com sucesso")
        self.assertEqual(PlantedTrees.objects.filter(user=self.user).count(), 4)


class PlantCreateServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="mahomes@chiefs.com",
            username="mahomes",
            is_superuser=True,
            is_staff=True,
        )
        cls.url = "api/plant/"

    def test_create_plan(self):
        view = PlantCreateService.as_view()
        payload = {
            "scientific_name": "science",
            "name": "new plant",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Planta criada com sucesso")

    def test_create_plan_blank(self):
        view = PlantCreateService.as_view()
        payload = {
            "scientific_name": "",
            "name": "",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["message"], "Nome e Nome Científico precisam ser adicionados"
        )

    def test_create_plan_not_superuser(self):
        view = PlantCreateService.as_view()
        user = User.objects.create(email="hurts@eagles.com", username="hurts")

        payload = {
            "scientific_name": "science",
            "name": "new plant",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PlantListVieweTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="mahomes@chiefs.com",
            username="mahomes",
            is_superuser=True,
            is_staff=True,
        )
        cls.user.set_password("123")
        Plant.objects.create(name="Plant1", scientific_name="Plant1")
        Plant.objects.create(name="Plant2", scientific_name="Plant2")
        Plant.objects.create(name="Plant3", scientific_name="Plant3")
        Plant.objects.create(name="Plant4", scientific_name="Plant4")
        cls.user.save()

    def test_get_plants_list(self):
        self.client.login(username="mahomes", password="123")
        response = self.client.get(reverse("tree:list_plant"))

        self.assertEqual(response.context["plants"].count(), 4)

    def test_get_create_plan(self):
        account = Account.objects.create(name="te")
        AccountUser.objects.create(account=account, user=self.user)

        self.client.login(username="mahomes", password="123")
        response = self.client.get(reverse("tree:tree_create"))

        self.assertEqual(response.context["plants"].count(), 4)
        self.assertEqual(response.context["accounts"].count(), 1)

    def test_get_plants_details(self):
        account = Account.objects.create(name="te")
        plant = Plant.objects.create(name="Plant1", scientific_name="Plant1")
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=1,
            latitude=1,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=2,
            latitude=2,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=3,
            latitude=3,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=4,
            latitude=4,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=5,
            latitude=1,
            longitude=22,
        )

        self.client.login(username="mahomes", password="123")
        response = self.client.get(reverse("tree:details_plant", args=[plant.id]))

        self.assertEqual(response.context["plants"].count(), 5)

    def test_get_accounts_details(self):
        account = Account.objects.create(name="te")
        plant = Plant.objects.create(name="Plant1", scientific_name="Plant1")
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=1,
            latitude=1,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=2,
            latitude=2,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=3,
            latitude=3,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=4,
            latitude=4,
            longitude=22,
        )
        PlantedTrees.objects.create(
            user=self.user,
            plant=plant,
            account=account,
            age=5,
            latitude=1,
            longitude=22,
        )

        self.client.login(username="mahomes", password="123")
        response = self.client.get(reverse("tree:tree_by_accounts", args=[account.id]))

        self.assertEqual(response.context["trees"].count(), 5)
