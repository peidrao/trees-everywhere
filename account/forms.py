from django import forms
from django.contrib.auth.forms import AuthenticationForm


class LoginAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, profile):
        if not profile.is_active:
            raise forms.ValidationError("This account is inactive")
        if profile.username.startswith("b"):
            raise forms.ValidationError(
                "Sorry, accounts starting with 'b' aren't welcome here."
            )
