from django.urls import path
from account.views import (
    ProfileUpdateService,
    UserCreateService,
    UserCreateView,
    AccountsListView,
    DashBoardProfileView,
    LoginView,
    LogoutView,
    UsersListView,
    AccountCreateService,
    AccountCreateView,
    ProfileUpdateView,
    AccountsMeView,
    AccountUpdateStatusService,
)

app_name = "account"

urlpatterns = [
    path("", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("dashboard/", DashBoardProfileView.as_view(), name="dashboard"),
    path("manager/accounts/", AccountsListView.as_view(), name="account_list"),
    path("manager/account/create/", AccountCreateView.as_view(), name="create_account"),
    path("manager/users/", UsersListView.as_view(), name="user_list"),
    path("manager/users/create", UserCreateView.as_view(), name="create_user"),
    path("accounts/me/", AccountsMeView.as_view(), name="account_me"),
    path("user/about/", ProfileUpdateView.as_view(), name="update_about"),
    path("api/users/", UserCreateService.as_view(), name="user_service"),
    path("api/accounts/", AccountCreateService.as_view(), name="account_service"),
    path(
        "api/accounts/<int:pk>/active/",
        AccountUpdateStatusService.as_view(),
        name="account_active",
    ),
    path(
        "api/users/update/", ProfileUpdateService.as_view(), name="update_about_service"
    ),
]
