from django.urls import reverse

from django.test import TestCase, Client
from account.models import Account, AccountUser, Plant, PlantedTrees, Profile
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from django.contrib.auth.models import User

from account.views import (
    AccountCreateService,
    AccountUpdateStatusService,
    UserCreateService,
    ProfileUpdateService,
)


class AccountsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.account1 = Account.objects.create(name="Chiefs")
        cls.account2 = Account.objects.create(name="Eagles")

        cls.user1 = User.objects.create(email="mahomes@chiefs.com", username="mahomes")
        cls.user1.set_password("123")
        cls.user1.save()
        cls.user2 = User.objects.create(email="hurts@eagles.com", username="hurts")
        cls.user3 = User.objects.create(email="kelce@eagles.com", username="kelce")

        AccountUser.objects.create(user=cls.user1, account=cls.account1)
        AccountUser.objects.create(user=cls.user1, account=cls.account2)
        AccountUser.objects.create(user=cls.user2, account=cls.account1)
        AccountUser.objects.create(user=cls.user2, account=cls.account2)
        AccountUser.objects.create(user=cls.user3, account=cls.account1)
        AccountUser.objects.create(user=cls.user3, account=cls.account2)

        cls.plant1 = Plant.objects.create(
            name="Planta 1", scientific_name="Planta Scientific 1"
        )
        cls.plant2 = Plant.objects.create(
            name="Planta 2", scientific_name="Planta Scientific 2"
        )
        cls.plant3 = Plant.objects.create(
            name="Planta 3", scientific_name="Planta Scientific 3"
        )

        PlantedTrees.objects.create(
            age=1,
            user=cls.user1,
            plant=cls.plant1,
            account=cls.account1,
            latitude=12,
            longitude=12,
        )
        PlantedTrees.objects.create(
            age=2,
            user=cls.user1,
            plant=cls.plant2,
            account=cls.account2,
            latitude=13,
            longitude=13,
        )
        PlantedTrees.objects.create(
            age=3,
            user=cls.user1,
            plant=cls.plant3,
            account=cls.account1,
            latitude=1,
            longitude=3,
        )
        PlantedTrees.objects.create(
            age=4,
            user=cls.user1,
            plant=cls.plant1,
            account=cls.account2,
            latitude=3,
            longitude=1,
        )

        PlantedTrees.objects.create(
            age=2,
            user=cls.user2,
            plant=cls.plant1,
            account=cls.account1,
            latitude=1,
            longitude=4,
        )
        PlantedTrees.objects.create(
            age=2,
            user=cls.user2,
            plant=cls.plant1,
            account=cls.account2,
            latitude=2,
            longitude=3,
        )
        PlantedTrees.objects.create(
            age=2,
            user=cls.user2,
            plant=cls.plant2,
            account=cls.account1,
            latitude=3,
            longitude=2,
        )
        PlantedTrees.objects.create(
            age=2,
            user=cls.user2,
            plant=cls.plant2,
            account=cls.account2,
            latitude=4,
            longitude=1,
        )

        PlantedTrees.objects.create(
            age=1,
            user=cls.user3,
            plant=cls.plant1,
            account=cls.account1,
            latitude=44,
            longitude=1121,
        )
        PlantedTrees.objects.create(
            age=1,
            user=cls.user3,
            plant=cls.plant2,
            account=cls.account1,
            latitude=33,
            longitude=22,
        )
        PlantedTrees.objects.create(
            age=1,
            user=cls.user3,
            plant=cls.plant3,
            account=cls.account1,
            latitude=66,
            longitude=233,
        )
        PlantedTrees.objects.create(
            age=1,
            user=cls.user3,
            plant=cls.plant3,
            account=cls.account2,
            latitude=9,
            longitude=12,
        )

    def test_get_me_trees(self):
        self.client.login(username="mahomes", password="123")
        response = self.client.get(reverse("tree:tree_me"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context["user"], self.user1)
        self.assertEqual(response.context["trees"].count(), 4)


class PlantedTreesMethodTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(email="test@test.com", username="test")
        cls.plant = Plant.objects.create(
            name="Planta", scientific_name="Planta Scientific"
        )
        cls.account = Account.objects.create(name="Hunter")
        AccountUser.objects.create(user=cls.user, account=cls.account)

    def test_plant_tree(self):
        self.user.plant_tree(self.plant.id, (12221, 2224), self.account.id, 1)

        self.assertTrue(PlantedTrees.objects.filter(user=self.user).exists())
        self.assertEqual(
            PlantedTrees.objects.filter(user=self.user, account=self.account).count(), 1
        )
        self.assertEqual(
            PlantedTrees.objects.get(user=self.user, account=self.account).age, 1
        )
        self.assertEqual(
            PlantedTrees.objects.get(user=self.user, account=self.account).latitude,
            12221,
        )
        self.assertEqual(
            PlantedTrees.objects.get(user=self.user, account=self.account).longitude,
            2224,
        )

    def test_plant_trees(self):
        self.user.plant_trees(
            self.plant.id,
            [(12221, 2224), (222, 2), (1, 3), (22, 4)],
            self.account.id,
            2,
        )

        self.assertTrue(PlantedTrees.objects.filter(user=self.user).exists())
        self.assertTrue(PlantedTrees.objects.filter(latitude=12221).exists())
        self.assertTrue(PlantedTrees.objects.filter(latitude=222).exists())
        self.assertTrue(PlantedTrees.objects.filter(latitude=1).exists())
        self.assertTrue(PlantedTrees.objects.filter(latitude=22).exists())
        self.assertEqual(PlantedTrees.objects.filter(user=self.user).count(), 4)


class DashBoardProfileViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(email="test@test.com", username="test")
        cls.user.set_password("123")
        cls.user.save()

    def test_get_dashboard_not_authoization(self):
        response = self.client.get(reverse("account:dashboard"))
        self.assertEqual(response.url, "/")

    def test_get_dashboard_user(self):
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:dashboard"))
        data = response.content.decode("utf-8")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('<a href="/dashboard/">Dashboard</a>', data)
        self.assertIn("Adicionar Planta", data)

    def test_get_dashboard_superuser(self):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:dashboard"))
        data = response.content.decode("utf-8")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('<a href="/dashboard/">Dashboard</a>', data)
        self.assertIn("Criar Conta", data)


class ProfileUpdateViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(email="test@test.com", username="test")
        Profile.objects.create(about="mvp", user=cls.user)
        cls.user.set_password("123")
        cls.user.save()

    def test_get_dashboard_user(self):
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:update_about"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("mvp", data)

    def test_get_dashboard_user_about_is_blank(self):
        user = User.objects.create(email="mahomes@test.com", username="mahomes")
        Profile.objects.create(user=user)
        user.set_password("mahomes")
        user.save()

        self.client.login(username="mahomes", password="mahomes")
        response = self.client.get(reverse("account:update_about"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('<textarea name="about" class="form-control"></textarea>', data)

    def test_get_dashboard_user_not_profile(self):
        user = User.objects.create(email="mahomes@test.com", username="mahomes")
        user.set_password("mahomes")
        user.save()

        self.client.login(username="mahomes", password="mahomes")
        response = self.client.get(reverse("account:update_about"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('<textarea name="about" class="form-control"></textarea>', data)


class AccountsMeViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(email="test@test.com", username="test")
        account1 = Account.objects.create(name="Account1")
        account2 = Account.objects.create(name="Account2")
        AccountUser.objects.create(account=account1, user=cls.user)
        AccountUser.objects.create(account=account2, user=cls.user)
        cls.user.set_password("123")
        cls.user.save()

    def test_get_me_accounts(self):
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:account_me"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context["accounts"].count(), 2)
        self.assertIn("Account1", data)
        self.assertIn("Account2", data)

    def test_get_me_any_accounts(self):
        user = User.objects.create(email="test@test.com", username="test1")
        user.set_password("123")
        user.save()
        self.client.login(username="test1", password="123")
        response = self.client.get(reverse("account:account_me"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.context["accounts"].count())


class AccountsListViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.user = User.objects.create(email="test@test.com", username="test")
        Account.objects.create(name="Account1")
        Account.objects.create(name="Account2")
        Account.objects.create(name="Account3")
        Account.objects.create(name="Account4")
        cls.user.set_password("123")
        cls.user.save()

    def test_get_accounts_list(self):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:account_list"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context["accounts"].count(), 4)
        self.assertIn("Account1", data)
        self.assertIn("Account2", data)
        self.assertIn("Account3", data)
        self.assertIn("Account4", data)

    def test_get_accounts_list_not_superuser(self):
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:account_list"))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, "/")


class AccountCreateViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.user = User.objects.create(email="test@test.com", username="test")
        cls.user.set_password("123")
        cls.user.save()

    def test_get_create_account(self):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:create_account"))
        data = response.content.decode("utf-8")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("Nome", data)
        self.assertIn("Adicionar Conta", data)

    def test_get_acreate_acount_not_superuser(self):
        self.client.login(username="test", password="123")
        response = self.client.get(reverse("account:create_account"))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, "/")


class UserCreateServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="test@test.com",
            username="test",
            is_staff=True,
            is_superuser=True,
            password="123",
        )
        cls.account1 = Account.objects.create(name="Account 1")
        cls.account2 = Account.objects.create(name="Account 2")
        cls.url = "api/users/"

    def test_create_user(self):
        view = UserCreateService.as_view()
        payload = {
            "name": "Test",
            "email": "purdy@gmail.com",
            "username": "purdy",
            "password": "123",
            "account": [self.account1.id, self.account2.id],
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Usuário cadastrado com sucesso")
        self.assertTrue(User.objects.filter(email="purdy@gmail.com").exists())

    def test_create_already_exists(self):
        view = UserCreateService.as_view()
        payload = {
            "name": "Test",
            "email": "purdy@gmail.com",
            "username": "test",
            "password": "123",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["message"], "Usuário já existe")

    def test_create_not_password(self):
        view = UserCreateService.as_view()
        payload = {
            "name": "Test",
            "email": "mahomes@gmail.com",
            "username": "mahomes",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["message"], "Necessário senha para cadastrar usuário"
        )

    def test_create_user_not_authorization(self):
        view = UserCreateService.as_view()
        request = self.factory.post(self.url, {}, format="json")
        force_authenticate(request)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class AccountCreateServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="test@test.com",
            username="test",
            is_staff=True,
            is_superuser=True,
            password="123",
        )
        cls.url = "api/accounts/"

    def test_create_account(self):
        view = AccountCreateService.as_view()
        payload = {
            "name": "new Account",
        }
        request = self.factory.post(self.url, payload, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "Conta criada com sucesso")


class AccountUpdateStatusServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="test@test.com",
            username="test",
            is_staff=True,
            is_superuser=True,
            password="123",
        )
        cls.url = "api/accounts/"

    def test_update_account_status_inactive(self):
        view = AccountUpdateStatusService.as_view()
        account = Account.objects.create(name="Chiefs")
        request = self.factory.post(f"api/accounts/{account.id}/active/", format="json")
        force_authenticate(request, user=self.user)
        response = view(request, pk=account.id)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["message"], "A conta foi desativada")
        self.assertTrue(Account.objects.filter(name="Chiefs", active=False).exists())

    def test_update_account_status_active(self):
        view = AccountUpdateStatusService.as_view()
        account = Account.objects.create(name="Eagles", active=False)
        request = self.factory.post(f"api/accounts/{account.id}/active/", format="json")
        force_authenticate(request, user=self.user)
        response = view(request, pk=account.id)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["message"], "A conta foi ativada")
        self.assertTrue(Account.objects.filter(name="Eagles", active=True).exists())

    def test_update_account_status_not_permission(self):
        view = AccountUpdateStatusService.as_view()
        account = Account.objects.create(name="Eagles", active=False)
        request = self.factory.post(f"api/accounts/{account.id}/active/", format="json")
        force_authenticate(request)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ProfileUpdateServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user = User.objects.create(
            email="test@test.com", username="test", password="123"
        )
        Profile.objects.create(about="Teste", user=cls.user)
        cls.url = "api/users/update/"

    def test_update_about_profile(self):
        view = ProfileUpdateService.as_view()
        payload = {"about": "Minha apresentacao"}
        request = self.factory.put(self.url, payload, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["message"], "Perfil atualizado com sucesso")
        self.assertEqual(
            Profile.objects.get(user=self.user).about, "Minha apresentacao"
        )

    def test_update_about_is_blank_profile(self):
        view = ProfileUpdateService.as_view()
        payload = {"about": ""}
        request = self.factory.put(self.url, payload, format="json")
        force_authenticate(request, user=self.user)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["message"], "Você precisa adicionar uma descrição sobre você."
        )
