from django.core.management.base import BaseCommand

from account.models import Account, Plant


class Command(BaseCommand):
    def handle(self, *args, **options):
        plants_list = [
            ("Abacateiro", "Persea americana"),
            ("Beterraba", "Beta vulgaris"),
            ("Boca de leão", "Antirrhinum majus"),
            ("Brócolis", "Brassica oleracea"),
            ("Cacau", "Theobroma cacao"),
            ("Cacto", "Cactus spp"),
            ("Cajueiro", "Anacardium occidentale"),
            ("Camomila", "Matricaria chamomilla"),
            ("Caquizeiro", "Diospyros kaki"),
            ("Castanheira do pará", "Bertholletia excelsa"),
        ]
        accounts_list = [
            "Amazônia",
            "Mata Atlântica",
            "Cerrado",
            "Caatinga",
            "Pampa",
            "Pantanal",
        ]

        for account in accounts_list:
            if not Account.objects.filter(name=account).exists():
                Account.objects.create(name=account)
        self.stdout.write(self.style.SUCCESS("Contas criadas"))

        for name, scientific_name in plants_list:
            if not Plant.objects.filter(name=name).exists():
                Plant.objects.create(name=name, scientific_name=scientific_name)
        self.stdout.write(self.style.SUCCESS("Plantas criadas"))
