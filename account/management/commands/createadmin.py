from django.core.management.base import BaseCommand

from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not User.objects.filter(username="admin").exists():
            user = User.objects.create(
                email="admin@admin.com.br",
                username="admin",
                first_name="Admin",
                is_superuser=True,
                is_staff=True,
                last_name="Test",
            )
            user.set_password("123")
            user.save()
