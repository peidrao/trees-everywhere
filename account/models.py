from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


def plant_tree(self, plant, location, account, age):
    PlantedTrees.objects.create(
        latitude=location[0],
        longitude=location[1],
        plant_id=plant,
        user=self,
        age=age,
        account_id=account,
    )


def plant_trees(self, plant, locations, account, age):
    for index in locations:
        PlantedTrees.objects.create(
            latitude=index[0],
            longitude=index[1],
            plant_id=plant,
            user=self,
            age=age,
            account_id=account,
        )


User.add_to_class("plant_tree", plant_tree)
User.add_to_class("plant_trees", plant_trees)


class Account(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)


class Profile(models.Model):
    about = models.TextField(blank=True, null=True)
    joined = models.DateTimeField(null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)


class AccountUser(models.Model):
    account = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)


class Plant(models.Model):
    name = models.CharField(max_length=255)
    scientific_name = models.CharField(max_length=255)


class PlantedTrees(models.Model):
    age = models.IntegerField()
    plated_at = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    plant = models.ForeignKey(Plant, on_delete=models.SET_NULL, null=True)
    account = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
