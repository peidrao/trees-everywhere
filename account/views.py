from django.contrib.auth import login, logout

from django.views import generic
from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from django.contrib.auth.models import User

from account.models import Account, AccountUser, Profile
from account.forms import LoginAuthenticationForm
from core.permissions import AdministratorPermission, LoginRequiredPermission


class LoginView(generic.View):
    template_name = "login.html"

    def post(self, request):
        form = LoginAuthenticationForm(request, data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            profile = Profile.objects.filter(user=user, joined__isnull=True)
            if profile.exists():
                profile = profile[0]
                profile.joined = timezone.now()
                profile.save()
            return HttpResponseRedirect(reverse("account:dashboard"))

    def get(self, request):
        return render(request, self.template_name, {})


class LogoutView(generic.RedirectView):
    pattern_name = "account:login"

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            logout(self.request)

        return super(LogoutView, self).get_redirect_url(*args, **kwargs)


class DashBoardProfileView(LoginRequiredPermission, generic.View):
    template_name = "dashboard.html"

    def get(self, request):
        return render(request, self.template_name, {})


class ProfileUpdateView(LoginRequiredPermission, generic.View):
    template_name = "profile/profile_update.html"

    def get(self, request):
        context = {}
        try:
            profile = Profile.objects.get(user=request.user)
            context["about"] = profile.about if profile.about != None else ""
        except Profile.DoesNotExist:
            context["about"] = ""

        return render(request, self.template_name, context)


class ProfileUpdateService(views.APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request):
        data = request.data
        about = data.get("about")

        if not about:
            return Response(
                {"message": "Você precisa adicionar uma descrição sobre você."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if Profile.objects.filter(user=request.user).exists():
            profile = Profile.objects.get(user=request.user)
            profile.about = about
            profile.save()

        return Response(
            {"message": "Perfil atualizado com sucesso"}, status=status.HTTP_200_OK
        )


class AccountsMeView(LoginRequiredPermission, generic.View):
    template_name = "profile/account_me.html"

    def get(self, request):
        accounts = Account.objects.filter(accountuser__user=request.user).distinct()

        return render(request, self.template_name, {"accounts": accounts})


class AccountsListView(AdministratorPermission, generic.ListView):
    queryset = Account.objects.all().order_by("id")
    template_name = "admin/account/account_table.html"

    def get(self, request):
        context = {"accounts": self.queryset.all()}
        return render(request, self.template_name, context)


class AccountCreateView(AdministratorPermission, generic.View):
    template_name = "admin/account/account_create.html"

    def get(self, request):
        return render(request, self.template_name, {})


class AccountCreateService(views.APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request):
        data = request.data
        name = data.get("name")

        Account.objects.create(name=name)

        return Response(
            {"message": "Conta criada com sucesso"}, status=status.HTTP_201_CREATED
        )


class AccountUpdateStatusService(views.APIView):
    queryset = Account.objects.all()
    permission_classes = (IsAdminUser,)

    def post(self, request, pk):
        try:
            account = self.queryset.get(id=pk)
        except Account.DoesNotExist:
            return Response(
                {"message": "A conta não foi encontrada"}, status.HTTP_404_NOT_FOUND
            )

        active = not account.active
        account.active = active
        account.save()

        active_message = "ativada" if account.active else "desativada"
        return Response(
            {"message": f"A conta foi {active_message}"}, status=status.HTTP_200_OK
        )


class UsersListView(AdministratorPermission, generic.ListView):
    queryset = User.objects.all().order_by("id")
    template_name = "admin/user/user_table.html"

    def get(self, request):
        context = {"users": self.queryset.all()}
        return render(request, self.template_name, context)


class UserCreateView(AdministratorPermission, generic.View):
    template_name = "admin/user/user_create.html"

    def get(self, request):
        accounts = Account.objects.filter(active=True).order_by("id")
        context = {"accounts": accounts}
        return render(request, self.template_name, context)


class UserCreateService(views.APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request):
        data = request.data
        email = data.get("email")
        username = data.get("username")
        first_name = data.get("first_name", "")
        password = data.get("password")

        try:
            accounts = data.getlist("account")
        except:
            accounts = data.get("account")

        if not password:
            return Response(
                {"message": "Necessário senha para cadastrar usuário"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if User.objects.filter(username=username).exists():
            return Response(
                {"message": "Usuário já existe"}, status=status.HTTP_400_BAD_REQUEST
            )

        user = User.objects.create(
            username=username, email=email, first_name=first_name
        )
        user.set_password(password)
        user.save()

        Profile.objects.create(user=user)
        for account in accounts:
            AccountUser.objects.create(user=user, account_id=account)

        return Response(
            {"message": "Usuário cadastrado com sucesso"},
            status=status.HTTP_201_CREATED,
        )
